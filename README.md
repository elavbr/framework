# Elav Framework

## Usage

     composer require elavbr/framework

## Application and Dependency Injection

### Controllers

### Serialization

## Credits
This application bundles Open Source components. You can find the source 
code of their open source projects along with license information below. 
We acknowledge and are grateful to these developers for their 
contributions to open source.

Project: Objectiveweb http://github.com/objectiveweb
Copyright 2011 Guilherme Barile
License (MIT) 

Project: Dice Depency Injection Container https://r.je/dice.html
Copyright 2012-2015 Tom Butler 
License (BSD License) http://www.opensource.org/licenses/bsd-license.php 
